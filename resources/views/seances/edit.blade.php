<x-app>
    <x-slot name="page_title">Edit {{ $seance->name . ' a ' . $seance->birthdate }}</x-slot>

    <form method="POST" action="{{ route('seances.update', $seances->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <p>
            <label for="birthdate">Date</label>
            <input type="text" name="birthdate" id="birthdate" value="{{ $artist->birthdate }}" class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-grey-darker border border-grey rounded" />
            <x-error name="birthdate"></x-error>
        </p>

        <button type="submit" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">
            Update
        </button>
    </form>
</x-app>
