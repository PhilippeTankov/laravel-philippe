<x-app>
    <x-slot name="page_title">Seances</x-slot>

    <a href="{{ route('artist.create') }}" class="align-left focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">
        {{ __('New') }}
    </a>

    <table class="divide-y divide-gray-200 mt-4">
        <thead class="bg-gray-50">
            <tr>
                <th class="text-left px-2 py-3">{{ __('Name') }}</th>
                <th class="text-left px-2 py-3">{{ __('Birthdate') }}</th>
            </tr>
        </thead>
        <tbody class="bg-white text-xs divide-y divide-gray-200">
            @foreach($seances as $seance)
            <tr>
                <td class="px-2 py-4">{{ $seance->name }}</td>
                <td class="px-2 py-4">{{ $seance->birthdate }}</td>
                <td class="px-2 py-4">
                    @can('update', $seance)
                    <a href="{{ route('seance.edit', $seance->name) }}" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">
                        {{ __('Edit') }}
                    </a>
                    @endcan

                    @can('delete', $seance)
                    <a href="{{ route('seance.destroy', $artist->name) }}" class="delete focus:outline-none text-white text-sm py-2.5 px-5 ml-2 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg">
                        {{ __('Delete') }}
                    </a>
                    @endcan
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $seances->links() }}

    <script>
    // Récupération du token
    let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    // Ajout des événements
    document.querySelectorAll('.delete').forEach(item => {
      item.addEventListener('click', event => {
        event.preventDefault();

        // Requête AJAX de suppression
        fetch(event.target.href, {
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': token
          },
          method: 'DELETE',
        })
        .then((data) => {
            event.target.closest('tr').remove();
        });
      })
    });
    </script>

</x-app>
