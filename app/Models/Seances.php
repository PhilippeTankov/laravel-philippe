<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seances extends Model
{
    use HasFactory;

    public function Cinemas_diffuse()
    {
        return $this->hasMany(Cinemas::class);
    }

    public function Salles_diffuse()
    {
        return $this->hasMany(Salles::class);
    }
}
