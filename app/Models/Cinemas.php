<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cinemas extends Model
{
    use HasFactory;

    public function salles_cinema()
    {
        return $this->hasMany(Salles::class);
    }

    public function films_diffuse()
    {
        return $this->hasMany(Movie::class);
    }
}
